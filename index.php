<?php namespace CrossBridgePuzzle;

    /**
     * @file Purpose of this script is to find a solution for bridge riddle:
     * @link https://www.youtube.com/watch?v=7yDmGnA8Hw0
     *
     * @author Vitaly Poletaev
     * @date 15/08/2016
     *
     * Idea is that we have 4 people who needs to cross a bridge within particular time, otherwise something bad will happen to them.
     * It will be simple if they can just walk all together, however there are few conditions to meet:
     *  1) Each person has particular has particular moving speed, so they will spend particular amount of time crossing the bridge
     *  2) No more than 2 people on the bridge at a time
     *  3) If 2 people are crossing - they need to move next to each other, so no simultaneous move from both sides are allowed
     *  4) Time on crossing should be equal or lower than deadline. So cases, when people started moving, but reached deadline mid way - failure
     *  5) No one should be left behind
     *  6) No magic, no rockets, no science - just walking over the bridge
     */


/**
 * Class CrossBridgeTask
 * @package CrossBridgePuzzle
 */
class CrossBridgeTask
{
    private $directions;            // Directions available for move. Was constant before, but didn't work on PHP 5.5 and lower
    private $participants;          // All of the people involved
    private $targetTime;            // Should be under this time to be able to survive
    private $safeCombinations;      // Combinations available for survival


    /**
     * CrossBridgeTask constructor.
     * Assigning the crowd, time limit and triggering combinations calculations
     *
     * @param array $participants
     * @param int $targetTime
     */
    function __construct($participants = array(), $targetTime = null)
    {
        // Wanted to do as constant, but apparently doesn't work PHP 5.5 and lower
        $this->directions = array(
          'hell' => array(
            'icon' => '<-',             // Purely graphical
            'capacity' => 1,            // Number of people able to move at once
            'rescuePoint' => false,     // If this is going towards 'safe' zone, where people needs to be assembled
          ),
          'safe' => array(
            'icon' => '->',
            'capacity' => 2,
            'rescuePoint' => true,
          ),
        );

        $this->setParticipants($participants);

        $this->setTargetTime($targetTime);

        $combinations = $this->calculateCombinations();
        $this->setCombinations($combinations);
    }


    //************ PRIVATE ************


    /**
     * Makes one move, evaluates if it's final move or not. If not - creates combinations for this move and then checks each combination recursively.
     *
     * @param array $combinations combination will be assigned to this array
     * @param string $direction current direction
     * @param int $totalTime total time spent so far on all moves
     * @param array $groupOfPeople1 group of people to move from
     * @param array $groupOfPeople2 group of people to move to
     * @param int $depth iteration depth, used for debugging
     * @return bool whether or not combination leads to success
     */
    private function _doMove(&$combinations, $direction, $totalTime, $groupOfPeople1, $groupOfPeople2, $depth)
    {
        // If we are still under time limit - keep searching
        if ($totalTime <= $this->targetTime) {

            // If all of the people were rescued and we are currently about to move back to danger zone - prevent it and mark combination as a solution
            $prevDirection = $this->_getMoveDirection($direction, true);
            if ($this->directions[$prevDirection]['rescuePoint'] && count($groupOfPeople1) == count($this->participants)) {
                $foundSolution = true;
                // Otherwise just keep moving people
            } else {
                $foundSolution = false;

                // Need to invert direction for the next move
                $nextDirection = $this->_getMoveDirection($direction);
                // Need to know how many people are allowed to move during this turn
                $numberOfPeopleCrossing = $this->_getNumberOfPeopleCrossing($direction);
                // Retrieve all of the possible combinations for available members and capacity
                $groupMembersCombinations = $this->_getPossibleCombinations($groupOfPeople1, $numberOfPeopleCrossing);

                // If some combinations are found, go check each one of them
                if (!empty($groupMembersCombinations)) {
                    foreach ($groupMembersCombinations as $groupOnTheMove) {
                        // Move people first, record time
                        $data = $this->_moveBetweenGroups($groupOnTheMove, $groupOfPeople1, $groupOfPeople2);
                        // Then save combination to the result array
                        $key = $this->_setNewCombination($combinations, $groupOnTheMove, $direction, $data['time'], $totalTime + $data['time']);
                        // Attempt next move in the reverse direction
                        if ($this->_doMove($combinations[$key], $nextDirection, $totalTime + $data['time'], $data['groupOfPeople2'], $data['groupOfPeople1'], ++$depth)) {
                            // And if solution found, mark entry, so we won't delete it when recursion will start to decrement
                            $foundSolution = true;
                        } else {
                            // Unset entry if combination has failed, like reached time limit.
                            unset($combinations[$key]);
                        }
                    }
                }
            }

            // If solution is found, make sure it's getting passed level up, so entry won't get unset by mistake
            if ($foundSolution) {
                return true;
            }
        }

        // Otherwise we are sure, that combination is not satisfying our requirement, so we can unset it.
        return false;
    }


    /**
     * Gives next / previous direction for movement. At this point it is doesn't really matter as there are only 2 places available,
     *  but might be useful if more places introduced.
     *
     * @TODO need to rewrite, so won't use hardcoded values
     *
     * @param string $direction current direction
     * @param bool $prevPoint do you need next or previous direction
     * @return string
     */
    private function _getMoveDirection($direction, $prevPoint = false)
    {
        if ($prevPoint) {
            if ($direction == 'safe') {
                $direction = 'hell';
            } else {
                $direction = 'safe';
            }
        } else {
            if ($direction == 'safe') {
                $direction = 'hell';
            } else {
                $direction = 'safe';
            }
        }

        return $direction;
    }


    /**
     * Returns capacity of the move
     *
     * @param string $direction current direction
     * @return int
     */
    private function _getNumberOfPeopleCrossing($direction)
    {
        return $this->directions[$direction]['capacity'];
    }


    /**
     * Retrieves a list of possible combinations for the specified list of people.
     *
     * @param array $groupOfPeople all of the people to move
     * @param int $capacity how many are capable of moving at a time
     * @return array
     */
    private function _getPossibleCombinations($groupOfPeople, $capacity)
    {
        $combinations = array();
        $totalPeople = count($groupOfPeople);

        if ($totalPeople <= $capacity) {
            $combinations[] = $groupOfPeople;
        } elseif ($capacity) {
            $this->_findPossibleCombinations($combinations, array(), $groupOfPeople, $capacity);
        }

        return $combinations;
    }


    /**
     * Constructs actual combinations. Assumption is made that the actual person place doesn't matter. So array(person1, person2) == array(person2, person1)
     * Also capacity needs to be filled completely. So group of 2 in capacity of 3 spaces is not a valid option.
     *
     * @param array $combinations will be populated with combinations
     * @param array $selected temporary storage of combinations while they are getting constructed
     * @param array $groupToTraverse group of people that will be used as a base to construct combinations
     * @param int $capacity how many are capable of moving at a time
     * @return array|bool array if combination is complete, bool otherwise
     */
    private function _findPossibleCombinations(&$combinations, $selected, $groupToTraverse, $capacity)
    {
        if ($capacity) {
            $capacity--;

            for ($i = 0; $i < count($groupToTraverse); $i++) {
                $selected[] = $groupToTraverse[$i];

                $combination = $this->_findPossibleCombinations($combinations, $selected, array_slice($groupToTraverse, $i + 1), $capacity);
                if (is_array($combination)) {
                    $combinations[] = $combination;
                }

                array_pop($selected);
            }

            return true;
        } else {
            return $selected;
        }
    }


    /**
     * Move people between locations
     *
     * @param array $groupOnTheMove people that are going to be moved
     * @param array $groupOfPeople1 will be moved from this group
     * @param array $groupOfPeople2 will be moved to this group
     * @return array information with updated group members and time spent on the move
     */
    private function _moveBetweenGroups($groupOnTheMove, $groupOfPeople1, $groupOfPeople2)
    {
        foreach ($groupOnTheMove as $individual) {
            $groupOfPeople1 = $this->_removeFrom($groupOfPeople1, $individual);
            $groupOfPeople2 = $this->_addTo($groupOfPeople2, $individual);
        }

        $time = $this->_getTimeForCrossing($groupOnTheMove);

        return array(
          'groupOfPeople1' => $groupOfPeople1,
          'groupOfPeople2' => $groupOfPeople2,
          'time' => $time,
        );
    }


    /**
     * Removes person from the group
     *
     * @param array $groupOfPeople
     * @param Person $individual
     * @return array
     */
    private function _removeFrom($groupOfPeople, $individual)
    {
        $leftovers = array();
        foreach ($groupOfPeople as $oneOfThem) {
            if ($oneOfThem != $individual) {
                $leftovers[] = $oneOfThem;
            }
        }

        return $leftovers;
    }


    /**
     * Adds person to the group
     *
     * @param array $groupOfPeople
     * @param Person $individual
     * @return array
     */
    private function _addTo($groupOfPeople, $individual)
    {
        if (!in_array($individual, $groupOfPeople)) {
            $groupOfPeople[] = $individual;
        }

        return $groupOfPeople;
    }


    /**
     * Calculates time for each move for the group. Slowest person in the group defining the group time
     *
     * @param array $groupOnTheMove
     * @return int
     */
    private function _getTimeForCrossing($groupOnTheMove)
    {
        $time = 0;

        foreach ($groupOnTheMove as $person) {
            if (isset($person->timeToCross) && $person->timeToCross > $time) {
                $time = $person->timeToCross;
            }
        }

        return $time;
    }


    /**
     * Sets new combination entry based on the information provided.
     *
     * @param array $combinations combination will be assigned to this array
     * @param array $groupOnTheMove people that are going to be moved
     * @param string $direction direction in which people was moved
     * @param int $time time spent on the current move
     * @param int $totalTime total time spent on all moves so far
     * @return string
     */
    private function _setNewCombination(&$combinations, $groupOnTheMove, $direction, $time, $totalTime)
    {
        $key = $this->_getKeyForTheGroup($groupOnTheMove);

        if ($key && !in_array($key, array_keys($combinations))) {
            $combinations[$key] = array(
              'details' => array(
                'moved' => $groupOnTheMove,
                'direction' => $this->directions[$direction]['icon'],
                'time_to_move' => $time,
                'total_time' => $totalTime,
              ),
            );
        }

        return $key;
    }


    /**
     * Constructing unique key for the group based on a group members id
     *
     * @param array $groupOnTheMove people that are going to be moved
     * @return string
     */
    private function _getKeyForTheGroup($groupOnTheMove)
    {
        $values = array();

        if (!empty($groupOnTheMove)) {
            foreach ($groupOnTheMove as $individual) {
                if (isset($individual->value)) {
                    $values[] = $individual->value;
                }
            }
        }

        return implode('_and_', $values);
    }


    /**
     * Will return nicely rendered combinations
     *
     * @return string
     */
    public function _getNicelyPrintedCombinations()
    {
        $output = array();

        if (php_sapi_name() == 'cli') {
            $break = PHP_EOL;
        } else {
            $break = '<br/>';
        }

        $this->_traverseCombinations($output, array(), $this->safeCombinations, 0);

        if (!empty($output)) {
            foreach ($output as $index => $item) {
                $output[$index] = implode($break, $item);
            }
        }

        return implode($break . $break, $output);
    }


    /**
     * In order to make nice output need to go through found combinations and extract necessary info.
     *
     * @param array $output will hold info for printing. 2 dimensional array of combinations of rows for each combination.
     * @param int $selected will hold selected combinations
     * @param array $combinations combinations found
     * @param int $depth iteration depth. For debugging purposed.
     */
    private function _traverseCombinations(&$output, $selected, $combinations, $depth)
    {
        // Going through each combination to render
        if (!empty($combinations)) {
            foreach ($combinations as $index => $combination) {
                // Least likely that details won't be set, but we want to be safe, don't we?
                if ($niceDetails = $this->_getNiceTitle($combination['details'])) {
                    $selected[] = $niceDetails;
                } else {
                    $selected[] = 'Can\'t fetch details for ' . $index . ' in depth ' . $depth;
                }

                // So it won't loop through details during next recursion - let's unset it
                if (isset($combination['details'])) {
                    unset($combination['details']);
                }

                // Going 1 level down and repeat the process
                $this->_traverseCombinations($output, $selected, $combination, $depth + 1);

                // Need to unset element which we've set previously, so it won't be duplicated
                array_pop($selected);
            }
        } else {
            // Only when we reach the very last element in the nested structure, we can say, that selection is complete
            $output[] = $selected;
        }
    }


    /**
     * Get nicely printed entry
     *
     * @param array $details detailst of a single combination entry
     * @return string
     */
    private function _getNiceTitle($details)
    {
        $titles = array();

        if (isset($details['moved'])) {
            foreach ($details['moved'] as $person) {
                $titles[] = $person->title;
            }

            return $details['direction'] .
            ' People moved: ' . implode(', ', $titles) .
            ' (move time: ' . $details['time_to_move'] .
            ' total time so far: ' . $details['total_time'] . ')';
        }

        return '';
    }


    //************ PUBLIC ************


    /**
     * Will calculate combinations, based on information assigned to the object
     *
     * @return array
     */
    public function calculateCombinations()
    {
        $combinations = array();
        $this->_doMove($combinations, 'safe', 0, $this->participants, array(), 0);
        return $combinations;
    }


    //************ Accessors ************


    /**
     * Sets array of combinations
     *
     * @param $combinations
     */
    public function setCombinations($combinations)
    {
        $this->safeCombinations = $combinations;
    }


    /**
     * Retrieves already calculated combinations
     *
     * @param bool $nice set to TRUE if you want output to be nicely printed
     * @return array|string array of combinations or HTML string if $nice is set to TRUE
     */
    public function getCombinations($nice = false)
    {
        if ($nice) {
            return $this->_getNicelyPrintedCombinations();
        } else {
            return $this->safeCombinations;
        }
    }


    /**
     * Sets people involved in the calculations.
     *
     * @param array $participants array of Person type objects
     */
    public function setParticipants($participants)
    {
        if (!empty($participants) && is_array($participants)) {
            /** @var Person $participant */
            foreach ($participants as $participant) {
                if (get_class($participant) == __NAMESPACE__ . '\Person') {
                    $this->participants[] = $participant;
                }
            }
        } else {
            $this->participants = array();
        }
    }


    /**
     * Sets target time for combinations search
     *
     * @param int $targetTime
     */
    public function setTargetTime($targetTime)
    {
        $this->targetTime = isset($targetTime) && is_int($targetTime) ? $targetTime : 0;
    }
}


/**
 * Class Person
 * @package CrossBridgePuzzle
 */
class Person
{
    public $title;          // User friendly title
    public $value;          // I guess we can call it id, probably bad name convention
    public $timeToCross;    // Time required for one move

    /**
     * Person constructor.
     * Just assign all of the parameters on build
     *
     * @param string $title user friendly title
     * @param string $value i guess we can call it id, probably bad name convention
     * @param int $timeToCross time required for one move
     */
    function __construct($title, $value, $timeToCross)
    {
        $this->title = is_string($title) ? $title : '';
        $this->value = is_string($value) ? $value : '';
        $this->timeToCross = is_int($timeToCross) ? $timeToCross : 0;
    }
}


/**
 * User friendly printing.
 *
 * @param mixed $vars
 */
function print_krumo($vars)
{
    $path = __DIR__ . DIRECTORY_SEPARATOR . 'krumo/class.krumo.php';
    if (is_file($path)) {
        require_once $path;

        if (function_exists('krumo') && (is_object($vars) || is_array($vars))) {
            krumo($vars);
        } else {
            print_r('<pre>' . $vars . '</pre>');
        }
    }
}


/**
 * Test set
 */
function cross_bridge_task()
{
    try {
        $data = array(
          new Person('You', 'you', 1),
          new Person('Lab Assistant', 'lab_assistant', 2),
          new Person('Janitor', 'janitor', 5),
          new Person('Professor', 'professor', 8),
        );

        $cbt = new CrossBridgeTask($data, 17);


        if (php_sapi_name() == 'cli') {
            echo($cbt->getCombinations(true) . PHP_EOL);
        } else {
            print_krumo($cbt->getCombinations(true));
            print_krumo($cbt->getCombinations());
        }

    } catch (\Exception $e) {
        echo('Error: ' . $e->getMessage());
    }
}

// Let's see if it works
cross_bridge_task();
