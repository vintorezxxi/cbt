Purpose of this script is to find a solution for bridge riddle:

https://www.youtube.com/watch?v=7yDmGnA8Hw0

> Idea is that we have 4 people who needs to cross a bridge within particular time, otherwise something bad will happen to them.<br/>
> It will be simple if they can just walk all together, however there are few conditions to meet:
> * Each person has particular has particular moving speed, so they will spend particular amount of time crossing the bridge
> * No more than 2 people on the bridge at a time
> * If 2 people are crossing - they need to move next to each other, so no simultaneous move from both sides are allowed
> * Time on crossing should be equal or lower than deadline. So cases, when people started moving, but reached deadline mid way - failure
> * No one should be left behind
> * No magic, no rockets, no science - just walking over the bridge
